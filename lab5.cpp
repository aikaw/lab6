/****************************
 Aika Washington
 aikaw
 Lab 5
 Lab Section: 001
 Nushrat Humaira
****************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <ctime>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

// creating a Card type struct
typedef struct Card {
  Suit suit;
  int value;
} Card;

// function prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
// int myrandom (int i) { return std::rand()%i;}
int myrandom (int i);

int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  int i, q;

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

  // creating the array of cards
  Card cardArray[52];

  //ace 14, king 13, queen 12, jack 11

  // creating spades cards
  q = 2;
  for(i=0; i < 13; i++){
    cardArray[i].suit = SPADES;
    cardArray[i].value = q;
  }

  // creating hearts cards, reseting value (q) variable
  q = 2;
  for(i=13;i < 26; i++){
    cardArray[i].suit = HEARTS;
    cardArray[i].value = q;
    q++;
  }

  // creating diamonds cards, reseting value (q) variable
  q = 2;
  for(i=26; i < 39; i++){
    cardArray[i].suit = DIAMONDS;
    cardArray[i].value = q;
    q++;
  }

  // creating clubs, reseting value (q) variable
  q = 2;
  for(i=39; i < 52; i++){
    cardArray[i].suit = CLUBS;
    cardArray[i].value = q;
    q++;
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   // using myrandom as the seed to shuffle
   random_shuffle(&cardArray[0], &cardArray[51], myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    Card hand[5] = {cardArray[0], cardArray[1], cardArray[2],cardArray[3],
    cardArray[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    // must pass suit ordering function so sort knows how to sort
     sort(hand,(hand+5), suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     // for loop to print the sorted hand of 5 cards
     for(i =0; i < 5; i++){
       cout <<  setw(10) << right << get_card_name(hand[i]) <<  " of " ;
       cout << get_suit_code(hand[i]) << endl;
     }

     return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
/*parameters - lhs - constant Card reference
*              rhs - constant Card refernce
*This function sorts the given cards by suit order
* the returns a true/false value depending on which
* of the two cards is bigger.
*
*
*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if( lhs.suit == rhs.suit){
    if(lhs.value < rhs.value){
      return true;
    }
  }

  // case for if suits are different
  if ( lhs.suit < rhs.suit){
    return true;
  }

  else
    return false;

}
/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

/*parameters - c - constant Card reference
* This function uses the enum values to decide
 *which character to show and returns it.
*
*
*/
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}


/*parameters - c - constant Card reference
* This function uses the card's value to decide
 *which word or to_string value to return.
*
*
*/
string get_card_name(Card& c) {
  //
  switch (c.value){
    case 11:       return "Jack";
    case 12:       return "Queen";
    case 13:       return "King";
    case 14:       return "Ace";
    default:       return  to_string(c.value);
  }
}

/*parameters - i - integer variable
* This function gets a "random" integer
* and returns its value % i.
*
*
*/
int myrandom (int i) {
  // getting a "random" integer
  return std::rand()%i;
}
